# Global configuration
$sourceZip = "https://git.doit.wisc.edu/JEREMY.MARITZ/2019benchmark/raw/master/doit2019baseline.zip"
$destinationZip = "C:\doit2019baseline.zip"
$extractPath = "C:\doit2019baseline\"
$localCommandPath = $extractPath
$localCommandInput = $extractPath + "input.txt"
$localCommand = "uwmadison-security_baseline.cmd"

# Download ZIP
try {
    $webClient = New-Object System.Net.WebClient
    $webClient.DownloadFile($sourceZip, $destinationZip)
    }
catch {
    
    Write-Host "Unable to download ""$sourceZip"" as ""$destinationZip"""
    # Write-Host $_.Exception.Message
    Exit 4
}

# Extract ZIP
try {
    # $shell = New-Object -ComObject Shell.Application
    # $shell.NameSpace($extractPath).CopyHere(($shell.NameSpace($destinationZip)).items())
    Add-Type -AssemblyName System.IO.Compression.FileSystem
    [System.IO.Compression.ZipFile]::ExtractToDirectory($destinationZip, $extractPath)
}
catch {
    Write-Host "Unable to extract ""$destinationZip"" into ""$extractPath"""
    # Write-Host $_.Exception.Message
    Exit 4
}

# Run post install configuration script
try {
    Start-Process -FilePath "$localCommandPath$localCommand" -WorkingDirectory "$localCommandPath" -RedirectStandardInput "$localCommandInput" -Wait
}
catch {
    Write-Host "Unable to execute ""$localCommand"" from ""$localCommandPath"""
    # Write-Host $_.Exception.Message
}

Remove-Item C:\doit2019baseline -Recurse
Remove-Item C:\doit2019baseline.zip
Remove-Item C:\doit2019baseline.ps1